{-# LANGUAGE DeriveGeneric #-}

module Schisma.CLI.Tracker
  ( PlayTrackerOptions(..)
  , CompositionFileJSON(..)
  , CsoundSettingsJSON(..)
  , InstrumentsFileJSON(..)
  , ProjectFileJSON(..)
  , TrackerSettingsJSON(..)
  , playTrackerOptionsParser
  , toInstruments
  , toInstrumentParameters
  ) where

import           Prelude                 hiding ( lookup )

import           Data.Aeson                     ( FromJSON )

import           GHC.Generics                   ( Generic )

import           Data.Text                      ( Text
                                                , unpack
                                                )

import           Data.Map.Strict                ( Map
                                                , empty
                                                , fromList
                                                , lookup
                                                )

import           Options.Applicative            ( Parser
                                                , auto
                                                , help
                                                , long
                                                , metavar
                                                , option
                                                , short
                                                , strOption
                                                )

import           Data.List                      ( foldl' )

import           Schisma.Csound.Types.Instruments
                                                ( Instrument )

import           Schisma.CLI.TrackerSettings    ( FrequencyMapperJSON )
import           Schisma.Synth.Instruments      ( midiProfit
                                                , midiSoundFontPlayer
                                                , profit
                                                , soundFontPlayer
                                                )

import           Schisma.Utilities              ( merge )

-- TODO: Doc

data TrackerSettingsJSON = TrackerSettingsJSON
  { parameterRenamings :: Map Text Text
  , frequencyMapper    :: FrequencyMapperJSON
  }
  deriving (Generic, Show)

newtype CsoundSettingsJSON = CsoundSettingsJSON
  { headerStatements :: Map Text Text
  }
  deriving (Generic, Show)

data CompositionFileJSON = CompositionFileJSON
  { trackerSettings :: TrackerSettingsJSON
  , csoundSettings  :: CsoundSettingsJSON
  }
  deriving (Generic, Show)

data InstrumentJSON = InstrumentJSON
  { name          :: Text
  , instrument    :: Text
  , soundFontPath :: Text
  , parameters    :: Map Text Double
  , midiChannel   :: Integer
  }
  deriving (Generic, Show)

newtype InstrumentsFileJSON = InstrumentsFileJSON
  { instruments        :: [InstrumentJSON]
  }
  deriving (Generic, Show)

data ProjectFileJSON = ProjectFileJSON
  { compositionFile :: FilePath
  , instrumentsFile :: FilePath
  , trackerFile     :: FilePath
  }
  deriving (Generic, Show)

instance FromJSON TrackerSettingsJSON
instance FromJSON CsoundSettingsJSON
instance FromJSON CompositionFileJSON
instance FromJSON InstrumentJSON
instance FromJSON InstrumentsFileJSON
instance FromJSON ProjectFileJSON


data PlayTrackerOptions = PlayTrackerOptions
  { projectFile  :: FilePath
  , startingLine :: Integer
  , endingLine   :: Integer
  }
  deriving Show


playTrackerOptionsParser :: Parser PlayTrackerOptions
playTrackerOptionsParser =
  PlayTrackerOptions
    <$> strOption
          (long "project-file" <> short 'p' <> metavar "FILENAME" <> help
            "Project file"
          )
    <*> option
          auto
          (  long "start"
          <> short 's'
          <> help "The starting line number"
          <> metavar "INT"
          )
    <*> option
          auto
          (long "end" <> short 'e' <> help "The ending line number" <> metavar
            "INT"
          )

toInstruments :: Map Text Integer -> InstrumentJSON -> [Instrument]
toInstruments instrumentNameNumbers json = schismaInstrument
 where
  instrumentName    = name json
  synth             = instrument json
  channel           = midiChannel json
  schismaInstrument = case lookup instrumentName instrumentNameNumbers of
    Nothing               -> []
    Just instrumentNumber -> case synth of
      "Profit" -> if channel /= 0
        then [profit instrumentNumber, midiProfit channel instrumentNumber]
        else [profit instrumentNumber]
      "SoundFont" -> if channel /= 0
        then
          [ soundFontPlayer (soundFontPath json) instrumentNumber
          , midiSoundFontPlayer channel instrumentNumber
          ]
        else [soundFontPlayer (soundFontPath json) instrumentNumber]
      _ -> error $ "Instrument '" ++ unpack synth ++ "' not found"

toInstrumentParameters
  :: Map Text Integer -> [InstrumentJSON] -> Map Integer (Map Text Double)
toInstrumentParameters instrumentNameNumbers = foldl' f empty
 where
  f instrumentParameters (InstrumentJSON name _ _ parameters _) =
    case lookup name instrumentNameNumbers of
      Nothing -> instrumentParameters
      Just number ->
        merge instrumentParameters $ fromList [(number, parameters)]
