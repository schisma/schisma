module Schisma.Tracker.Types where

import           Data.Map.Strict                ( Map )
import           Data.Text                      ( Text )

import           Schisma.Csound.Types.Instruments
                                                ( Instrument )
import           Schisma.Csound.Types.Score     ( Sound )

data NoteCell
  = NoteOn Text (Map Text Double)
  --       pitch instrument effects
  | NoteOff
  | NoteBlank (Map Text Double)
  --          instrument effects
  deriving (Show, Ord, Eq)

data MasterSettingsState = MasterSettingsState
  { trackerTuning         :: Text
  , trackerTemperament    :: Text
  , trackerBeatsPerMinute :: Double
  , trackerLinesPerBeat   :: Double
  }
  deriving (Show, Ord, Eq)

data MasterSetting
  = BeatsPerMinute Double
  | LinesPerBeat Double
  | Tuning Text
  | Temperament Text
  deriving (Show, Ord, Eq)

data MasterCell
  = MasterSettings [MasterSetting]
  | MasterBlankSettings
  deriving (Show, Ord, Eq)

data HeaderCell
  = LineNumberHeader
  | MasterHeader
  | InstrumentHeader Text    Bool  Bool
  --                 name    muted soloed
  deriving (Show, Ord, Eq)

data InstrumentTrack = InstrumentTrack
  { trackerTrackInstrumentNumber :: Integer
  , trackerTrackIsMute           :: Bool
  , trackerTrackIsSolo           :: Bool
  , trackerTrackNotes            :: [NoteCell]
  }
  deriving (Show, Ord, Eq)

data CellMappers = CellMappers
  { cellFrequencyMapper  :: Integer -> Text -> Text -> Text -> Map Text Double
  , cellParameterRenamer :: Map Text Double -> Map Text Double
  }

data Tracker = Tracker
  { trackerLineNumberTrack       :: [Integer]
  , trackerMasterTrack           :: [MasterCell]
  , trackerInstrumentTracks      :: [InstrumentTrack]
  , trackerCellMappers           :: CellMappers
  , trackerInstruments           :: [Instrument]
  , trackerInstrumentNameNumbers :: Map Text Integer
  , trackerInstrumentParameters  :: Map Integer (Map Text Double)
  , trackerLineConstraints       :: (Integer, Integer)
  }

data TrackerPlaybackState = TrackerPlaybackState
  { trackerPlaybackTimeElapsed              :: Double
  , trackerPlaybackPriorSounds              :: [Sound]
  , trackerPlaybackPriorLineNumber          :: Integer
  , trackerPlaybackPriorMasterSettingsState :: MasterSettingsState
  , trackerPriorNoteConcluded               :: Bool
  , trackerPriorPitch                       :: Maybe Text
  }
