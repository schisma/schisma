module Schisma.Tracker.Parser
  ( parseTrackerFile
  , parseTracker
  ) where

import           Control.Monad                  ( void )

import           Data.List                      ( (\\)
                                                , elemIndex
                                                , foldl'
                                                , transpose
                                                , zip3
                                                )
import           Data.Maybe                     ( fromMaybe )

import qualified Data.Map.Strict                ( Map
                                                , empty
                                                , fromList
                                                , lookup
                                                , size
                                                )

import           Data.Text                      ( Text
                                                , append
                                                , empty
                                                , isPrefixOf
                                                , lines
                                                , pack
                                                , split
                                                , strip
                                                )
import           Data.Text.IO                   ( readFile )

import           Data.Void                      ( Void )

import           Text.Megaparsec                ( (<|>)
                                                , Parsec
                                                , choice
                                                , errorBundlePretty
                                                , many
                                                , optional
                                                , parse
                                                , sepBy
                                                , some
                                                , try
                                                )
import           Text.Megaparsec.Char           ( alphaNumChar
                                                , char
                                                , digitChar
                                                , letterChar
                                                , printChar
                                                , spaceChar
                                                , string
                                                , upperChar
                                                )
import           Text.Megaparsec.Char.Lexer     ( decimal )

import           Schisma.Tracker.Mappers        ( defaultFrequencyMapper )
import           Schisma.Tracker.Types

import           Schisma.Utilities              ( mergeLeft )


type Parser = Parsec Void Text

-- | Parses the contents within the supplied @file@ as a Tracker.
parseTrackerFile
  :: FilePath   -- ^ @file@ - The file.
  -> IO Tracker -- ^ The parsed Tracker.
parseTrackerFile file = do
  contents <- Data.Text.IO.readFile file
  pure (parseTracker contents)

-- | Parses the supplied text @contents@ as a Tracker.
parseTracker
  :: Text    -- ^ @contents@ - The contents.
  -> Tracker -- ^ The parsed Tracker.
parseTracker contents = tracker where
  allRows = Data.Text.lines contents
  removeJunk line = not ("-" `isPrefixOf` line)
  rows          = filter removeJunk allRows

  headerColumns = lineToColumns $ head rows
  header        = parseHeader headerColumns

  tracks        = case rows of
    []     -> []
    x : xs -> Data.List.transpose $ map lineToColumns xs

  columns = zip header tracks
  (maybeMasterTrack, maybeLineNumberTrack, instrumentTracks, instrumentNameNumbers)
    = toTracks columns

  lineNumberTrack =
    fromMaybe (error "No LineNumberHeader column found") maybeLineNumberTrack
  masterTrack =
    fromMaybe (error "No MasterHeader column found") maybeMasterTrack

  cellMappers = CellMappers
    { cellFrequencyMapper  = defaultFrequencyMapper
    , cellParameterRenamer = const Data.Map.Strict.empty
    }

  tracker = Tracker { trackerLineNumberTrack       = lineNumberTrack
                    , trackerMasterTrack           = masterTrack
                    , trackerInstrumentTracks      = instrumentTracks
                    , trackerCellMappers           = cellMappers
                    , trackerInstruments           = []
                    , trackerInstrumentNameNumbers = instrumentNameNumbers
                    , trackerInstrumentParameters  = Data.Map.Strict.empty
                    , trackerLineConstraints       = (-1, -1)
                    }

toTracks
  :: [(HeaderCell, [Text])]
  -> ( Maybe [MasterCell]
     , Maybe [Integer]
     , [InstrumentTrack]
     , Data.Map.Strict.Map Text Integer
     )
toTracks = foldl' f (Nothing, Nothing, [], Data.Map.Strict.empty) where
  f (maybeMasterTrack, maybeLineNumberTrack, instrumentTracks, instrumentNameNumbers) (headerRow, bodyRows)
    = case headerRow of
      MasterHeader ->
        ( Just $ parseMasterTrack bodyRows
        , maybeLineNumberTrack
        , instrumentTracks
        , instrumentNameNumbers
        )
      LineNumberHeader ->
        ( maybeMasterTrack
        , Just $ parseLineNumberTrack bodyRows
        , instrumentTracks
        , instrumentNameNumbers
        )
      InstrumentHeader name mute solo ->
        let number = case Data.Map.Strict.lookup name instrumentNameNumbers of
              Just n -> n
              Nothing ->
                fromIntegral $ Data.Map.Strict.size instrumentNameNumbers + 1
            cells = parseInstrumentTrack bodyRows
        in  ( maybeMasterTrack
            , maybeLineNumberTrack
            , InstrumentTrack number mute solo cells : instrumentTracks
            , mergeLeft instrumentNameNumbers
                        (Data.Map.Strict.fromList [(name, number)])
            )

lineToColumns :: Text -> [Text]
lineToColumns = split (== ',')

parseCell :: Parser a -> Text -> a
parseCell cellParser cell = case parse cellParser "" (strip cell) of
  Left  bundle  -> error (errorBundlePretty bundle)
  Right results -> results

parseMasterTrack :: [Text] -> [MasterCell]
parseMasterTrack = map (parseCell parseMasterCell)

parseLineNumberTrack :: [Text] -> [Integer]
parseLineNumberTrack = map (parseCell integerLiteral)

parseInstrumentTrack :: [Text] -> [NoteCell]
parseInstrumentTrack = map (parseCell parseNoteCell)

parseHeader :: [Text] -> [HeaderCell]
parseHeader = map (parseCell parseHeaderCell)

integerLiteral :: Parser Integer
integerLiteral = decimal

doubleLiteral :: Parser Double
doubleLiteral = read <$> (negativeParser <|> decimalParser <|> integerParser) where
  decimalParser :: Parser String
  decimalParser = try $ do
    front <- some digitChar
    void $ char '.'
    back <- some digitChar
    pure $ front ++ ('.' : back)

  integerParser :: Parser String
  integerParser = try $ some digitChar

  negativeParser :: Parser String
  negativeParser = do
    void $ char '-'
    num <- decimalParser <|> integerParser
    pure $ '-' : num

parseInstrumentEffect :: Parser (Text, Double)
parseInstrumentEffect = do
  name <- some letterChar
  void $ char ':'
  value <- doubleLiteral
  pure (pack name, value)

parsePitch :: Parser Text
parsePitch = do
  name       <- some upperChar
  accidental <- many $ choice [char 'b', char '#']
  octave     <- some digitChar
  pure $ pack (name ++ accidental ++ octave)

parseNoteOn :: Parser NoteCell
parseNoteOn = do
  pitch   <- parsePitch
  effects <- optional $ do
    void spaceChar
    sepBy parseInstrumentEffect spaceChar

  case effects of
    Nothing -> pure (NoteOn pitch Data.Map.Strict.empty)
    Just fx -> pure (NoteOn pitch $ Data.Map.Strict.fromList fx)

parseNoteOff :: Parser NoteCell
parseNoteOff = do
  void $ string "OFF"
  pure NoteOff

parseNoteBlank :: Parser NoteCell
parseNoteBlank = do
  effects <- optional $ sepBy parseInstrumentEffect spaceChar

  case effects of
    Nothing -> pure (NoteBlank Data.Map.Strict.empty)
    Just fx -> pure (NoteBlank $ Data.Map.Strict.fromList fx)

parseNoteCell :: Parser NoteCell
parseNoteCell = parseNoteOff <|> parseNoteOn <|> parseNoteBlank

parseBeatsPerMinute :: Parser MasterSetting
parseBeatsPerMinute = do
  void $ string "bpm:"
  BeatsPerMinute <$> doubleLiteral

parseLinesPerBeat :: Parser MasterSetting
parseLinesPerBeat = do
  void $ string "lpb:"
  LinesPerBeat <$> doubleLiteral

parseTuning :: Parser MasterSetting
parseTuning = do
  void $ choice [string "tn:", string "tuning:"]
  tuning <- some alphaNumChar
  pure (Tuning (pack tuning))

parseTemperament :: Parser MasterSetting
parseTemperament = do
  void $ choice [string "tm:", string "temperament:"]
  temperament <- some alphaNumChar
  pure (Temperament (pack temperament))

parseMasterSetting :: Parser MasterSetting
parseMasterSetting =
  parseBeatsPerMinute <|> parseLinesPerBeat <|> parseTuning <|> parseTemperament

parseMasterCell :: Parser MasterCell
parseMasterCell = do
  settings <- sepBy parseMasterSetting spaceChar
  if null settings
    then pure MasterBlankSettings
    else pure (MasterSettings settings)

parseLineNumberHeader :: Parser HeaderCell
parseLineNumberHeader = do
  void $ char '#'
  pure LineNumberHeader

parseMasterHeader :: Parser HeaderCell
parseMasterHeader = do
  void $ string "Master"
  pure MasterHeader

parseInstrumentHeader :: Parser HeaderCell
parseInstrumentHeader = do
  muteOrSolo <- optional $ choice [char 'M', char 'S']
  void $ char 'I'
  void spaceChar
  name <- some printChar

  let (mute, solo) = case muteOrSolo of
        Just 'M' -> (True, False)
        Just 'S' -> (False, True)
        _        -> (False, False)

  pure (InstrumentHeader (pack name) mute solo)

parseHeaderCell :: Parser HeaderCell
parseHeaderCell =
  try parseInstrumentHeader <|> parseMasterHeader <|> parseLineNumberHeader
