module Main where

import           Prelude                 hiding ( concat
                                                , putStrLn
                                                )


import           Options.Applicative            ( (<**>)
                                                , Parser
                                                , command
                                                , execParser
                                                , fullDesc
                                                , header
                                                , helper
                                                , hsubparser
                                                , info
                                                , progDesc
                                                )

import           Data.ByteString.Lazy.Char8     ( putStrLn )

import           Data.Aeson                     ( eitherDecodeFileStrict'
                                                , encode
                                                )

import           Data.Map.Strict                ( empty )
import           Data.Maybe                     ( fromMaybe )

import           System.Directory               ( canonicalizePath )
import           System.FilePath                ( (</>)
                                                , isAbsolute
                                                , takeDirectory
                                                )


import           Schisma.Csound.Types.Instruments
                                                ( Instrument(instrumentNumber) )


import           Schisma.Tracker.Parser         ( parseTrackerFile )
import           Schisma.Tracker.Types          ( CellMappers(..)
                                                , Tracker(..)
                                                )
import           Schisma.Utilities              ( renameKeysFromMap )

import           Schisma.CLI.Synth              ( synths )
import           Schisma.CLI.Tracker            ( CompositionFileJSON(..)
                                                , CsoundSettingsJSON(..)
                                                , InstrumentsFileJSON(..)
                                                , PlayTrackerOptions(..)
                                                , ProjectFileJSON(..)
                                                , TrackerSettingsJSON(..)
                                                , playTrackerOptionsParser
                                                , toInstrumentParameters
                                                , toInstruments
                                                )
import           Schisma.CLI.TrackerSettings    ( toFrequencyMapper )
import           Schisma.IO                     ( playTracker )

-- TODO: Doc

data CLICommand
  = TrackerCLI TrackerCommand
  | SynthCLI SynthCommand
  deriving (Show)

newtype TrackerCommand = Play PlayTrackerOptions
  deriving Show

data SynthCommand = SynthList
  deriving Show


main :: IO ()
main = run =<< execParser opts
 where
  opts = info
    (parser <**> helper)
    (fullDesc <> progDesc "A lightweight composition tool." <> header "schisma")

run :: CLICommand -> IO ()
run (TrackerCLI trackerCommand) = case trackerCommand of
  (Play options) -> playTrackerFile options
run (SynthCLI synthCommand) = case synthCommand of
  SynthList -> printSynthParameters

parser :: Parser CLICommand
parser = hsubparser
  (  command "tracker" (info trackerParser (progDesc "Work with trackers"))
  <> command "synth"   (info synthParser (progDesc "Work with synths"))
  )

synthParser :: Parser CLICommand
synthParser = SynthCLI <$> hsubparser
  (command
    "list"
    (info (pure SynthList) (progDesc "Returns the list of available synths"))
  )

trackerParser :: Parser CLICommand
trackerParser = TrackerCLI <$> hsubparser
  (command
    "play"
    (info (Play <$> playTrackerOptionsParser)
          (progDesc "Compiles and runs the supplied tracker file")
    )
  )

printSynthParameters :: IO ()
printSynthParameters = putStrLn $ encode synths

playTrackerFile :: PlayTrackerOptions -> IO ()
playTrackerFile (PlayTrackerOptions projectFile startingLine endingLine) = do
  projectFileConfig <-
    eitherDecodeFileStrict' projectFile :: IO (Either String ProjectFileJSON)
  let projectConfig = case projectFileConfig of
        Left  message -> error ("Invalid project file. " ++ message)
        Right json    -> json

  trackerFilePath <- resolveProjectComponentFilePath projectFile
    $ trackerFile projectConfig
  tracker             <- parseTrackerFile trackerFilePath

  instrumentsFilePath <- resolveProjectComponentFilePath projectFile
    $ instrumentsFile projectConfig

  instrumentsFileConfig <-
    eitherDecodeFileStrict' instrumentsFilePath :: IO
      (Either String InstrumentsFileJSON)
  let instrumentsConfig = case instrumentsFileConfig of
        Left  message -> error ("Invalid instruments file. " ++ message)
        Right json    -> json

  compositionFilePath <- resolveProjectComponentFilePath projectFile
    $ compositionFile projectConfig

  compositionFileConfig <-
    eitherDecodeFileStrict' compositionFilePath :: IO
      (Either String CompositionFileJSON)
  let compositionConfig = case compositionFileConfig of
        Left  message -> error ("Invalid composition file. " ++ message)
        Right json    -> json

  let csoundConfig = csoundSettings compositionConfig
  let trackerConfig = trackerSettings compositionConfig

  let cellMappers = CellMappers
      -- TODO: The frequency mapper should have a track number
      -- associated with it so that it can be applied differently across
      -- tracks
        { cellFrequencyMapper  = toFrequencyMapper
                                   (frequencyMapper trackerConfig)
        , cellParameterRenamer = renameKeysFromMap
                                   (parameterRenamings trackerConfig)
        }

  let instrumentsJson       = instruments instrumentsConfig
  let instrumentNameNumbers = trackerInstrumentNameNumbers tracker

  let instruments =
        concatMap (toInstruments instrumentNameNumbers) instrumentsJson
  let instrumentParameters =
        toInstrumentParameters instrumentNameNumbers instrumentsJson
  let lineConstraints = (startingLine, endingLine)

  let tracker' = tracker { trackerCellMappers          = cellMappers
                         , trackerInstruments          = instruments
                         , trackerInstrumentParameters = instrumentParameters
                         , trackerLineConstraints      = lineConstraints
                         }

  playTracker (headerStatements csoundConfig) tracker'

resolveProjectComponentFilePath :: FilePath -> FilePath -> IO FilePath
resolveProjectComponentFilePath projectFile projectComponentFile =
  if isAbsolute projectComponentFile
    then pure projectComponentFile
    else do
      canonicalizePath (takeDirectory projectFile </> projectComponentFile)
